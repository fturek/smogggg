package com.slowiak.turek.smoG.controller;


import com.slowiak.turek.smoG.model.Station;
import com.slowiak.turek.smoG.repository.StationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StationsController {
    private StationsRepository stationsRepository;

    @Autowired
    public StationsController(StationsRepository stationsRepository) {
        this.stationsRepository = stationsRepository;
    }

    @GetMapping(name = "/stations")
    public List<Station> getAllStations(){
        return stationsRepository.findAll();
    }


}
