FROM openjdk:8-jdk-alpine
VOLUME /tmp
CMD java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar build/libs/smog-0.0.1-SNAPSHOT.jar
